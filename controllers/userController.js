const db = require('../models')


const user = db.user
const Review = db.review

// main work

// 1. create product

const createUser = async (req, res) => {

    let info = {
      customer_name:req.body.userName,
      mobile_number:req.body.mobileNumber,
      email:req.body.email,
      address:req.body.address,
    } 

    const result = await user.create(info)
    res.status(200).send(result)
    console.log(result)

}



// 2. get all products

const getAllUsers = async (req, res) => {

    let users = await user.findAll({})
    res.status(200).send(users)

}

// 3. get single product

const getUserById = async (req, res) => {

    let id = req.params.userId
    let userData = await user.findOne({ where: { id: id }})
    res.status(200).send(userData)

}

// 4. update Product

const updateUserById = async (req, res) => {

    let id = req.params.userId
 let info={
     customer_name:req.body.userName,
    mobile_number:req.body.mobileNumber,
    email:req.body.email,
    address:req.body.address
}
    const userData = await user.update(info, { where: { id: id }})

    res.status(200).send(userData)
   

}

// 5. delete product by id

const deleteUserById = async (req, res) => {

    let id = req.params.userId
    
    await user.destroy({ where: { id: id }} )

    res.status(200).send('Product is deleted !')

}

const userReview=async(req,res)=>{ 
    
    const data = await user.findAll({
    include: [{
        model: Review,
        as: 'customereview'
    }],
  
})
res.status(200).send(data);
}





module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById,
    userReview
}