const db = require('../models')


const Review = db.review



const createReview = async (req, res) => {

    let info = {
      customer_id:req.body.customerId,
      product_id:req.body.productId,
      reviews:req.body.reviews,
      stars:req.body.stars,
    } 

    const result = await Review.create(info)
    res.status(200).send(result)
    console.log(result)

}



const getAllReviews = async (req, res) => {

    let result = await Review.findAll({})
    res.status(200).send(result)

}


const getReviewById = async (req, res) => {

    let id = req.params.reviewId
    let userData = await Review.findOne({ where: { id: id }})
    res.status(200).send(userData)

}

const updateReviewById = async (req, res) => {

    let id = req.params.reviewId
    let info = {
        customer_id:req.body.customerId,
        product_id:req.body.productId,
        reviews:req.body.reviews,
        stars:req.body.stars,
      } 
  
    const result = await Review.update(info, { where: { id: id }})

    res.status(200).send(result)
   

}


const deleteReviewById = async (req, res) => {

    let id = req.params.reviewId
    
    await Review.destroy({ where: { id: id }} )

    res.status(200).send('Product is deleted !')

}







module.exports = {
    createReview,
    getAllReviews,
    getReviewById,
    updateReviewById,
    deleteReviewById,
}