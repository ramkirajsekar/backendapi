const db = require('../models')
const Order = db.order
const orderProducts=db.orderProduct
const createOrder = async (req, res) => {

    let info = {
        customer_id :req.body.customer_id ,
        date:req.body.date,
        customer_name:req.body.customer_name,
        customer_address:req.body.customer_address,
       
        shipping_method:req.body.shipping_method,
        total_amount:req.body.total_amount,
        gst_amount:req.body.gst_amount,
        shipping_charge:req.body.shipping_charge,
        grant_total:req.body.grant_total


    } 
    

    const result = await Order.create(info);
    const orderId = result.id;
    const products = req.body.products;

    const orderProduct = await Promise.all(products.map(async (product) => {
        const productInfo = {
            order_id: orderId,
            product_id: product.product_id,
            quantity: product.quantity,
            price: product.price,
            total_amount: product.total_amount,
        };

        return await orderProducts.create(productInfo);
    }));

    res.status(200).send(result);
    console.log(result)

}



const getAllOrders = async (req, res) => {

    let result = await Order.findAll({})
    res.status(200).send(result)

}

const getOrderById = async (req, res) => {

    let id = req.params.orderId
    let result = await [Order].findOne({ where: { id: id }})
    res.status(200).send(result)

}

const updateOrderById = async (req, res) => {

    let id = req.params.orderId
 let info={
    customer_id :req.body.customer_id ,
    date:req.body.date,
    customer_name:req.body.customer_name,
    customer_address:req.body.customer_address,
   
    shipping_method:req.body.shipping_method,
    total_amount:req.body.total_amount,
    gst_amount:req.body.gst_amount,
    shipping_charge:req.body.shipping_charge,
    grant_total:req.body.grant_total
}
    const result = await Order.update(info, { where: { id: id }})

    res.status(200).send(result)
   

}

const deleteOrderById = async (req, res) => {

    let id = req.params.orderId
    
    await Order.destroy({ where: { id: id }} )

    res.status(200).send('Product is deleted !')

}







module.exports = {
    createOrder,
    getAllOrders,
    getOrderById,
    updateOrderById,
    deleteOrderById,
}