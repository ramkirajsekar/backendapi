const db = require('../models')

const subCategory = db.subCategory
const createSubCategory = async (req, res) => {

    let info = {
        main_category_id :req.body.mainCategoryId ,
        sub_category_name:req.body.subCategoryName,
        sort_order:req.body.sortOrder,
        created_by:req.body.createdBy	
      
    } 

    const result = await subCategory.create(info)
    res.status(200).send(result)
    console.log(result)

}



// 2. get all products

const getAllSubCategory= async (req, res) => {

    let result = await subCategory.findAll({})
    res.status(200).send(result)

}

// 3. get single product

const getSubCategoryById = async (req, res) => {

    let id = req.params.subcategoryId
    let result = await subCategory.findOne({ where: { id: id }})
    res.status(200).send(result)

}

// 4. update Product

const updateSubCategoryById = async (req, res) => {

    let id = req.params.subcategoryId
    let info = {
        main_category_id :req.body.mainCategoryId ,
        sub_category_name:req.body.subCategoryName,
        sort_order:req.body.sortOrder,
        created_by:req.body.createdBy	
      
    } 
    const result = await subCategory.update(info, { where: { id: id }})

    res.status(200).send(result)
   

}

// 5. delete product by id

const deleteSubCategoryById = async (req, res) => {

    let id = req.params.subcategoryId
    
    await mainCategory.destroy({ where: { id: id }} )

    res.status(200).send('Main Category is deleted !')

}







module.exports = {
    createSubCategory,
    getAllSubCategory,
    getSubCategoryById,
    updateSubCategoryById,
    deleteSubCategoryById,
}