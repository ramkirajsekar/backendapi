const db = require('../models')
const Product = db.products;
const Review = db.review
const createProduct = async (req, res) => {

    let info = {
       main_category_id:req.body.main_category_id,
       sub_category_id:req.body.sub_category_id,
       product_id:req.body.product_id,
       product_name:req.body.product_name,
       product_description:req.body.product_description,
       
       product_image:req.body.product_image,
       selling_price:req.body.selling_price,
       offer_price:req.body.offer_price,
       sku:req.body.sku,
       created_by:req.body.created_by


    } 

    const product = await Product.create(info)
    res.status(200).send(product)
    console.log(product)

}



// 2. get all products

const getAllProducts = async (req, res) => {

    let products = await Product.findAll({})
    res.status(200).send(products)

}

// 3. get single product

const getProductById = async (req, res) => {

    let id = req.params.productId
    let result = await Product.findOne({ where: { id: id }})
    res.status(200).send(result)

}

// 4. update Product

const updateProductById = async (req, res) => {

    let id = req.params.productId
 let info={
    main_category_id:req.body.main_category_id,
       sub_category_id:req.body.sub_category_id,
       product_id:req.body.product_id,
       product_name:req.body.product_name,
       product_description:req.body.product_description,
       
       product_image:req.body.product_image,
       selling_price:req.body.selling_price,
       offer_price:req.body.offer_price,
       sku:req.body.sku,
       created_by:req.body.created_by
}
    const result = await Product.update(info, { where: { id: id }})

    res.status(200).send(result)
   

}

// 5. delete product by id

const deleteProductById = async (req, res) => {

    let id = req.params.productId
    
    await Product.destroy({ where: { id: id }} )

    res.status(200).send('Product is deleted !')

}



const productReview=async(req,res)=>{

      
    const data = await Product.findAll({
        include: [{
            model: Review,
            as: 'customereview'
        }],
      
    })
    res.status(200).send(data);
}



module.exports = {
    createProduct,
    getAllProducts,
    getProductById,
    updateProductById,
    deleteProductById,
    productReview
    
}