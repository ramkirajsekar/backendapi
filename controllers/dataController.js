const db = require('../models')
const Product = db.products
const reviews = db.review
const orderProduct=db.orderProduct
const {Sequelize} = require('sequelize');


const getTopProducts = async (req, res) => {

    let products = await reviews.findAll({where: {
        stars: {
          [Sequelize.Op.gte]: 3, 
        },
      },order: [
        ['stars', 'DESC'], 
      ],})
    res.status(200).send(products)

}



const bestsellers = async (req, res) => {

  let result=  await Product.findAll({
        attributes: [
          'product_id',
          'product_name',
          [Sequelize.fn('SUM', Sequelize.col('quantity')), 'totalSales'],
        ],
        include: [{
            model: orderProduct,
            as: 'Product'
        }],
        group: ['Products.product_id'],
        order: [[Sequelize.literal('totalSales'), 'DESC']],
        limit: 10,
      });
      res.status(200).send(result);
}



module.exports = {
   
    getTopProducts,
    bestsellers

}