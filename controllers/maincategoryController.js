const db = require('../models')
const mainCategory = db.mainCategory
const createMainCategory = async (req, res) => {

    let info = {
        main_category_name:req.body.main_category_name,
        sort_order:req.body.sort_order,
        created_by:req.body.created_by
      
    } 

    const result = await mainCategory.create(info)
    res.status(200).send(result)
    console.log(result)

}



// 2. get all products

const getAllMainCategory= async (req, res) => {

    let result = await mainCategory.findAll({})
    res.status(200).send(result)

}

// 3. get single product

const getMainCategoryById = async (req, res) => {

    let id = req.params.maincategoryId
    let result = await mainCategory.findOne({ where: { id: id }})
    res.status(200).send(result)

}

// 4. update Product

const updateMainCategoryById = async (req, res) => {

    let id = req.params.maincategoryId
 let info={
    main_category_name:req.body.main_category_name,
    sort_order:req.body.sort_order,
    created_by:req.body.created_by
  
}
    const result = await mainCategory.update(info, { where: { id: id }})

    res.status(200).send(result)
   

}

// 5. delete product by id

const deleteMainCategoryById = async (req, res) => {

    let id = req.params.maincategoryId
    
    await mainCategory.destroy({ where: { id: id }} )

    res.status(200).send('Main Category is deleted !')

}







module.exports = {
    createMainCategory,
    getAllMainCategory,
    getMainCategoryById,
    updateMainCategoryById,
    deleteMainCategoryById,
}