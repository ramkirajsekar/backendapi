module.exports = (sequelize, DataTypes) => {

    const user = sequelize.define("customers", {
        customer_name: {
            type: DataTypes.STRING
        },
        mobile_number: {
            type: DataTypes.STRING,
           
        },
        email: {
            type: DataTypes.STRING
        },
        address: {
            type: DataTypes.STRING
        },
       
    })

    return user

}