module.exports = (sequelize, DataTypes) => {

    const subCategory = sequelize.define("sub_category", {
        main_category_id: {
            type: DataTypes.INTEGER
        },
        sub_category_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        sort_order: {
            type: DataTypes.STRING
        },
       
        created_by: {
            type: DataTypes.STRING
        },
    })

    return subCategory

}