module.exports = (sequelize, DataTypes) => {

    const orders = sequelize.define("orders", {
        customer_id: {
            type: DataTypes.INTEGER
        },
        date: {
            type: DataTypes.DATE,
    
            allowNull: false
        },
        customer_name: {
            type: DataTypes.STRING
        },
       
        customer_address: {
            type: DataTypes.STRING
        },
        shipping_method: {
            type: DataTypes.STRING
        },
        total_amount: {
            type: DataTypes.STRING,
    
        },
        gst_amount: {
            type: DataTypes.STRING
        },
       
        shipping_charge: {
            type: DataTypes.STRING
        },
        
        grand_total: {
            type: DataTypes.STRING
        },
    })

    return orders

}