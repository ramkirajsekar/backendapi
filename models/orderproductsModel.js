module.exports = (sequelize, DataTypes) => {

    const orderProducts = sequelize.define("order_products", {
        order_id: {
            type: DataTypes.INTEGER
        },
        product_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        quantity: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.STRING
        },
        total_amount: {
            type: DataTypes.STRING
        },
       
    })

    return orderProducts

}