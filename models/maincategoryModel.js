module.exports = (sequelize, DataTypes) => {

    const MainCategory = sequelize.define("main_category", {
        main_category_name: {
            type: DataTypes.STRING
        },
        sort_order: {
            type: DataTypes.STRING,
            allowNull: false
        },
        
        created_by: {
            type: DataTypes.STRING
        },
    })

    return MainCategory

}