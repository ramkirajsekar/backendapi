module.exports = (sequelize, DataTypes) => {

    const review = sequelize.define("customer_reviews", {
        customer_id: {
            type: DataTypes.INTEGER
        },
        product_id: {
            type: DataTypes.INTEGER,
           
        },
        reviews: {
            type: DataTypes.STRING
        },
        stars: {
            type: DataTypes.STRING
        },
       
    })

    return review

}