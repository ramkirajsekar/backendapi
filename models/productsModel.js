module.exports = (sequelize, DataTypes) => {

    const products = sequelize.define("products", {
        main_category_id: {
            type: DataTypes.INTEGER
        },
        sub_category_id: {
            type: DataTypes.INTEGER,
           
        },
        product_name: {
            type: DataTypes.STRING
        },
        product_id: {
            type: DataTypes.INTEGER
        },
        product_description: {
            type: DataTypes.STRING
        },
        product_image: {
            type: DataTypes.STRING
        },
        selling_price: {
            type: DataTypes.STRING
        },
        offer_price: {
            type: DataTypes.STRING
        },
        sku: {
            type: DataTypes.STRING
        },
        created_by: {
            type: DataTypes.STRING
        },
       
    })

    return products

}