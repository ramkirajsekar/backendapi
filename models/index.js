const dbConfig = require('../config/db.js');

const {Sequelize, DataTypes} = require('sequelize');

const sequelize = new Sequelize(
    dbConfig.DB,
    dbConfig.USER,
    dbConfig.PASSWORD, {
        host: dbConfig.HOST,
        dialect: dbConfig.dialect,
        operatorsAliases: false,

        pool: {
            max: dbConfig.pool.max,
            min: dbConfig.pool.min,
            acquire: dbConfig.pool.acquire,
            idle: dbConfig.pool.idle

        }
    }
)

sequelize.authenticate()
.then(() => {
    console.log('connected..')
})
.catch(err => {
    console.log('Error'+ err)
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.user = require('./userModel.js')(sequelize, DataTypes)
db.products = require('./productsModel.js')(sequelize, DataTypes)
db.order = require('./ordersModel.js')(sequelize, DataTypes)
db.orderProduct = require('./orderproductsModel.js')(sequelize, DataTypes)
db.review = require('./reviewModel.js')(sequelize, DataTypes)
db.subCategory = require('./subcategoryModel.js')(sequelize, DataTypes)
db.mainCategory = require('./maincategoryModel.js')(sequelize, DataTypes)


db.sequelize.sync({ force: false })
.then(() => {
    console.log('yes re-sync done!')
})



// 1 to Many Relation

// db.products.hasMany(db.reviews, {
//     foreignKey: 'product_id',
//     as: 'review'
// })

// db.review.belongsTo(db.products, {
//     foreignKey: 'product_id',
//     as: 'product'
// })

db.user.hasMany(db.review,{
    foreignKey:'customer_id',
    as:'customereview'
})
db.products.hasMany(db.review,{
    foreignKey:'product_id',
    as:'customereview'
})


db.user.hasMany(db.order,{
    foreignKey:'customer_id',
    as:'Customerorders'
})

db.orderProduct.hasMany(db.order,{
    foreignKey:'order_id',
    as:'order'
})
db.products.hasMany(db.orderProduct,{
    foreignKey:'product_id',
    as:'Product'
})

db.products.hasOne(db.mainCategory,{
    foreignKey:'main_category_id',
    as:'main_category'
})


db.products.hasOne(db.subCategory,{
    foreignKey:'sub_category_id',
    as:'sub_category'
})

db.subCategory.hasOne(db.mainCategory,{
    foreignKey:'main_category_id',
    as:'main_category'
})

module.exports = db
