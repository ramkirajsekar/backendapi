const router = require('express').Router();


const userController=require('../controllers/userController.js');
const productController=require('../controllers/productController.js');
const orderController=require('../controllers/orderController.js');
const mainCategoryController=require('../controllers/maincategoryController.js');
const subCategoryController=require('../controllers/subcategoryController.js');
const reviewController=require('../controllers/reviewController.js');

const dataController=require("../controllers/dataController.js");
router.route('/users')
  .get(userController.getAllUsers)
  .post(userController.createUser);

router.route('/users/:userId')
  .get(userController.getUserById)
  .put(userController.updateUserById)
  .delete(userController.deleteUserById);

  router.route('/products')
  .get(productController.getAllProducts)
  .post(productController.createProduct);

router.route('/products/:productId')
  .get(productController.getProductById)
  .put(productController.updateProductById)
  .delete(productController.deleteProductById);

  router.route('/orders')
  .get(orderController.getAllOrders)
  .post(orderController.createOrder);

router.route('/orders/:orderId')
  .get(orderController.getOrderById)
  .put(orderController.updateOrderById)
  .delete(orderController.deleteOrderById);

  
  router.route('/maincategory')
  .get(mainCategoryController.getAllMainCategory)
  .post(mainCategoryController.createMainCategory);

router.route('/maincategory/:maincategoryId')
  .get(mainCategoryController.getMainCategoryById)
  .put(mainCategoryController.updateMainCategoryById)
  .delete(mainCategoryController.deleteMainCategoryById);

  
  router.route('/subcategory')
  .get(subCategoryController.getAllSubCategory)
  .post(subCategoryController.createSubCategory);

router.route('/subcategory/:subcategoryId')
  .get(subCategoryController.getSubCategoryById)
  .put(subCategoryController.updateSubCategoryById)
  .delete(subCategoryController.deleteSubCategoryById);

  router.route('/review')
  .get(reviewController.getAllReviews)
  .post(reviewController.createReview);

router.route('/review/:reviewId')
  .get(reviewController.getReviewById)
  .put(reviewController.updateReviewById)
  .delete(reviewController.deleteReviewById);

  router.route('/toprated').get( dataController.getTopProducts);
  router.route('/usersreview').get( userController.userReview);
  router.route('/productsreview').get( productController.productReview);
  router.route('/bestsellers').get(dataController.bestsellers );
  

  module.exports = router;